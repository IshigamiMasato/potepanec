require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:parent_taxon) { taxonomy.root }
    let(:child_taxon) { create(:taxon, parent: parent_taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [child_taxon]) }

    before { get potepan_category_path parent_taxon.id }

    it "レスポンスが成功する" do
      expect(response).to have_http_status(:success)
    end

    it "taxonomy名を取得する" do
      expect(response.body).to include taxonomy.name
    end

    it "parent_taxon名を取得する" do
      expect(response.body).to include parent_taxon.name
    end

    it "child_taxon名を取得する" do
      expect(response.body).to include child_taxon.name
    end

    it "商品名を取得する" do
      expect(response.body).to include product.name
    end

    it "商品価格を取得する" do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
