require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before { get potepan_product_path product.id }

    it "レスポンスが成功する" do
      expect(response).to have_http_status(:success)
    end

    it "商品名を取得する" do
      expect(response.body).to include product.name
    end

    it "商品価格を取得する" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品説明を取得する" do
      expect(response.body).to include product.description
    end

    it "関連商品名を取得する" do
      expect(response.body).to include related_product.name
    end

    it "関連商品価格を取得する" do
      expect(response.body).to include related_product.display_price.to_s
    end
  end
end
