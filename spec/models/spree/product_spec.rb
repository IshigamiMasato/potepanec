require 'rails_helper'

RSpec.describe "Spree::Product", type: :model do
  describe "#related_products" do
    subject { product.related_products }

    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon, name: "Apache") }
    let(:taxon3) { create(:taxon, name: "Ruby") }
    let(:product) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:related_product) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:unrelated_product) { create(:product, price: "20", taxons: [taxon3]) }

    it "関連商品を重複なく取得する" do
      is_expected.to match [related_product]
    end

    it "関連商品からレシーバーのレコードを除いている" do
      is_expected.not_to include product
    end

    it "関連商品でないものは取得しない" do
      is_expected.not_to include unrelated_product
    end
  end
end
