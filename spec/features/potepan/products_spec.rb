require 'rails_helper'

RSpec.describe "商品詳細ページ", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:parent_taxon) { taxonomy.root }
  let(:child_taxon1) { create(:taxon, parent: parent_taxon, taxonomy: taxonomy) }
  let(:child_taxon2) { create(:taxon, name: "Apache", parent: parent_taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [child_taxon1]) }
  let!(:related_product) { create(:product, taxons: [child_taxon1]) }
  let!(:related_products) { create_list(:product, 5, taxons: [child_taxon1]) }
  let!(:unrelated_product) { create(:product, price: "20", taxons: [child_taxon2]) }

  before { visit potepan_product_path product.id }

  describe "画面表示される商品情報" do
    describe "主要商品" do
      it "画面3点に商品名が表示される" do
        expect(page).to have_selector 'h2.title-product-name', text: product.name
        expect(page).to have_selector 'li', text: product.name
        expect(page).to have_selector 'h2.main-product-name', text: product.name
      end

      it "商品価格,説明が表示される" do
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    describe "関連商品" do
      it "関連商品が表示される" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end

      it "関連しない商品は表示しない" do
        expect(page).not_to have_content unrelated_product.name
        expect(page).not_to have_content unrelated_product.display_price
      end

      it "関連商品が4点表示される" do
        expect(page).to have_selector '.productBox', count: 4
      end
    end
  end

  describe "画面遷移" do
    subject { current_path }

    describe "topページへの画面遷移" do
      it "ヘッダーのBIGBAGロゴをクリックするとtopページに移動する" do
        click_on 'title-logo'
        is_expected.to eq potepan_index_path
      end

      it "ヘッダーのHOMEリンクをクリックするとtopページに移動する" do
        click_on 'HOME'
        is_expected.to eq potepan_index_path
      end

      it "LIGHT SECTIONのHOMEリンクをクリックするとtopページに移動する" do
        click_on 'Home'
        is_expected.to eq potepan_index_path
      end
    end

    describe "カテゴリーページへの画面遷移" do
      it "[一覧ページへ戻る]をクリックすると商品に紐づいたtaxonのカテゴリーページに移動する" do
        click_on '一覧ページへ戻る'
        is_expected.to eq potepan_category_path child_taxon1.id
      end
    end

    describe "商品詳細ページへの画面遷移" do
      it "関連商品をクリックすると、その商品の商品詳細ページへ移動する" do
        click_on related_product.name
        is_expected.to eq potepan_product_path related_product.id
      end
    end
  end
end
