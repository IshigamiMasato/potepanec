require 'rails_helper'

RSpec.describe "カテゴリーページ", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:parent_taxon) { taxonomy.root }
  let(:child_taxon1) { create(:taxon, parent: parent_taxon, taxonomy: taxonomy) }
  let(:child_taxon2) { create(:taxon, name: "Apache", parent: parent_taxon, taxonomy: taxonomy) }
  let!(:product1) { create(:product, taxons: [child_taxon1]) }
  let!(:product2) { create(:product, price: "20", taxons: [child_taxon2]) }

  before { visit potepan_category_path child_taxon1.id }

  describe "カテゴリーページに表示される情報" do
    it "LIGHT SECTIONに正しいtaxon名が2点表示される" do
      expect(page).to have_selector 'h2.title-taxon-name', text: child_taxon1.name
      expect(page).to have_selector 'li.active', text: child_taxon1.name
      expect(page).not_to have_selector 'h2.title-taxon-name', text: child_taxon2.name
      expect(page).not_to have_selector 'li.active', text: child_taxon2.name
      expect(page).not_to have_selector 'h2.title-taxon-name', text: parent_taxon.name
      expect(page).not_to have_selector 'li.active', text: parent_taxon.name
    end

    it "カテゴリーページにtaxonに紐づいた正しい商品一覧が表示される" do
      within ".productFrame" do
        expect(page).to have_link product1.name
        expect(page).to have_link product1.display_price
        expect(page).not_to have_link product2.name
        expect(page).not_to have_link product2.display_price
      end
    end

    it "サイドバーにtaxonomy名とchild_taxon名が表示される" do
      within ".side-nav" do
        expect(page).to have_link taxonomy.name
        expect(page).to have_link child_taxon1.name
        expect(page).to have_link child_taxon2.name
      end
    end
  end

  describe "画面遷移" do
    subject { current_path }

    describe "topページへの画面遷移" do
      it "LIGHT SECTIONのHomeリンクを押すとtopページに移動する" do
        click_on 'Home'
        is_expected.to eq potepan_index_path
      end
    end

    describe "商品詳細ページへの画面遷移" do
      it "商品名をクリックすると商品詳細ページに移動する" do
        click_on product1.name
        is_expected.to eq potepan_product_path product1.id
      end

      it "商品価格をクリックすると商品詳細ページに移動する" do
        click_on product1.display_price
        is_expected.to eq potepan_product_path product1.id
      end
    end

    describe "カテゴリーページへの画面遷移" do
      it "サイドバーの小項目child_taxon1をクリックするとその項目のカテゴリーページに移動する" do
        click_on child_taxon1.name
        is_expected.to eq potepan_category_path child_taxon1.id
      end

      it "サイドバーの小項目child_taxon2をクリックするとその項目のカテゴリーページに移動する" do
        click_on child_taxon2.name
        is_expected.to eq potepan_category_path child_taxon2.id
      end
    end
  end
end
