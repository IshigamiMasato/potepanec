require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    context "page_titleが空白の場合" do
      it "titleがBIGBAG storeになる" do
        expect(full_title("")).to eq "BIGBAG store"
      end
    end

    context "page_titleがnilの場合" do
      it "titleがBIGBAG storeになる" do
        expect(full_title(nil)).to eq "BIGBAG store"
      end
    end

    context "page_titleが存在する場合" do
      it "titleがsample - BIGBAG storeになる" do
        expect(full_title("sample")).to eq "sample-BIGBAG store"
      end
    end
  end
end
