module Potepan::ProductDecorator
  Spree::Product.class_eval do
    def related_products
      Spree::Product.in_taxons(taxons).where.not(id: id).distinct
    end
  end
end
